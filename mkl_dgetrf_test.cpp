#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include <sys/time.h>
#include<mkl_service.h>


#ifdef __cplusplus
extern "C" {
#endif
void dlarnv_( const int *idist, int *iseed, const int *n, double *x );
void dgetrf_( const int *m, const int *n, double *A, int *lda, int *ipiv, int *info );
void dgetf2_( const int *m, const int *n, double *A, int *lda, int *ipiv, int *info );
#ifdef __cplusplus
}
#endif

// timing function
extern "C"
double timer( void )
{
    struct timeval t;
    gettimeofday( &t, NULL );
    return t.tv_sec + t.tv_usec*1e-6;
}

// FLOPS
#define FMULS_GETRF(m_, n_) ( ((m_) < (n_)) \
		    ? (0.5 * (m_) * ((m_) * ((n_) - (1./3.) * (m_) - 1. ) + (n_)) + (2. / 3.) * (m_)) \
		    : (0.5 * (n_) * ((n_) * ((m_) - (1./3.) * (n_) - 1. ) + (m_)) + (2. / 3.) * (n_)) )
#define FADDS_GETRF(m_, n_) ( ((m_) < (n_)) \
		    ? (0.5 * (m_) * ((m_) * ((n_) - (1./3.) * (m_)      ) - (n_)) + (1. / 6.) * (m_)) \
		    : (0.5 * (n_) * ((n_) * ((m_) - (1./3.) * (n_)      ) - (m_)) + (1. / 6.) * (n_)) )
#define FLOPS_DGETRF(m_, n_) (FMULS_GETRF((double)(m_), (double)(n_)) + FADDS_GETRF((double)(m_), (double)(n_)))

int main(int argc, char* argv[]) 
{
    int m = 1000;
    int n = 1000;
    int niter = 1;
    int info[1] = {-1};

    if( argc > 1 ) {
        m = atoi( argv[1] );
    }
    
    if( argc > 2 ) {
        n = atoi( argv[2] );
    }

	if(argc > 3) {
        niter = atoi( argv[3] );
    }
    
    int nthreads = mkl_get_max_threads( );
    MKLVersion mkl_version;
	mkl_get_version( &mkl_version );
	printf( "# MKL version %d.%d.%d\n",mkl_version.MajorVersion, mkl_version.MinorVersion, mkl_version.UpdateVersion);
	printf( "# ---------------------------\n");
	//printf("\n ====> Testing for size %lld x %lld -- with %lld thread(s)\n", 
    //       (long long)m, (long long)n, (long long)nthreads);

    // allocate and generate random matrix
    int lda      = m;
    int min_mn   = std::min(m, n);
    int idist    = 0;
    int iseed[4] = {0, 0, 0, 1};
    int sizeA    = (lda*n);
    double *A    = NULL;
    int    *ipiv = NULL;
    for(int iter = 0; iter < niter; ++iter) {
		A    = (double*)malloc( sizeA * sizeof(double) );
		ipiv = (int*)malloc( min_mn * sizeof(int) );
		dlarnv_( &idist, iseed, &sizeA, A);
		double flops = FLOPS_DGETRF(m, n);
		
		double time = timer();
		dgetrf_( &m, &n, A, &lda, ipiv, info );
		//dgetf2_( &m, &n, A, &lda, ipiv, info );
		time = timer() - time;
		double gflops = flops / (time * 1e9);
		if( (niter > 1 && iter == 0) || (niter > 2 && iter < 2) ) {
		    printf("#"); // for warmup runs, gnuplot ignores lines starting with '#'
		}
		else {
		    printf(" "); // for alignment
		}
		printf(" %5d   %5d   %8.4f seconds   %10.4f Gflop/s   %3d threads\n", m, n, time, gflops, nthreads);
		
		if( A ) free(A);
		if( ipiv ) free(ipiv);
	}
    return 0;
}
