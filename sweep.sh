
######## Only edit these variables ############
dim_start=1000
dim_stop=20000
dim_step=1000
niter=20
cpu=skylake
tofile=0
###############################################

string1=$(grep INTEL_MKL_VERSION $MKLROOT/include/mkl_version.h)
string2=( $string1 )
MKLVER=${string2[2]}

for nb in 256 512
	do
		echo "# Testing Panels of width " ${nb}
		echo "# ==============================="
		
		fname=dgetrf_nb${nb}_${cpu}_mkl_${MKLVER}.txt
		rm -f $fname
		
		for((n=${dim_start};n<=${dim_stop};n=n+${dim_step}))
			do
		        if [ $tofile -eq 0 ]
				then
				    ./mkl_dgetrf_test ${n} ${nb} ${niter}
				else
					./mkl_dgetrf_test ${n} ${nb} ${niter} >> $fname
				fi

				if [ $niter -gt 1 ]
				then
					echo "#"
				fi
			done
			echo
	done


echo "# Testing Square Matrices"
echo "# ======================="

fname=dgetrf_sq_${cpu}_mkl_${MKLVER}.txt
rm -f $fname

for((n=${dim_start};n<=${dim_stop};n=n+${dim_step}))
	do
		if [ $tofile -eq 0 ]
		then
			./mkl_dgetrf_test ${n} ${n} ${niter} 
		else
		    ./mkl_dgetrf_test ${n} ${n} ${niter} >> $fname
		fi

		if [ $niter -gt 1 ] 
		then
			echo "#"
		fi
	done
echo


