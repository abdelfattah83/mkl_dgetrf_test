
# choose icx or icpc
CXX=icx
#CXX=icpc


src=mkl_dgetrf_test.cpp
obj=$(patsubst %.cpp,%.o,$(src))
exe=$(patsubst %.o,%,$(obj))

LIBDIR=-L${MKLROOT}/lib/intel64
LIB=-lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5

all:
	${CXX} -c ${src} -o ${obj}
	${CXX} -o ${exe} ${obj} ${LIBDIR} ${LIB}

clean:
	rm -f ${obj} ${exe}
